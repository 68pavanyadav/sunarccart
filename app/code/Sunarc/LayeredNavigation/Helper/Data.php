<?php

namespace Sunarc\LayeredNavigation\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{

    const CONFIG_PATH_MODULE_ENABLED = 'layerednavigation/general/enable';
    const CONFIG_PATH_MODULE_HIDE = 'layerednavigation/general/showselected';

    /*
     * check if module enabled
     */
    public function getConfigModuleEnabled()
    {
        return $this->scopeConfig->getValue(self::CONFIG_PATH_MODULE_ENABLED, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getConfigHideEnabled()
    {
        return $this->scopeConfig->getValue(self::CONFIG_PATH_MODULE_HIDE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
