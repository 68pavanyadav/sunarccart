<?php
namespace Sunarc\LayeredNavigation\Block\LayeredNavigation;

use Magento\Swatches\Block\LayeredNavigation\RenderLayered as CoreRender;
use Magento\Catalog\Model\Layer\Filter\FilterInterface;

/**
 * Class RenderLayered Render Swatches at Layered Navigation
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class RenderLayered extends CoreRender
{
    /**
     * @param string $attributeCode
     * @param int $optionId
     * @return string
     */
    protected $_template = 'Sunarc_LayeredNavigation::product/layered/renderer.phtml';

    public function buildUrl($attributeCode, $optionId)
    {

        $value = $this->filter->getValueAsArray();
        $value[] = $optionId;
        $result = array_unique($value);
        if (count($value) == 3 && count($result) == 1) {
            unset($value[2]);
        }
        if ($attributeCode == 'color') {
            $new_array_color[] = [];

            $i = 0;
            foreach ($value as $key => $val) {
                if ($val) {
                    if (!in_array($val, $new_array_color)) {
                        $new_array_color[$i] = $val;
                    } else {
                        $new_array_color = array_diff($new_array_color, [$val]);
                    }
                }
                $i++;
            }
            $value = implode('_', $new_array_color);
        } else {
            $value = implode('_', $value);
        }

        $query = [$attributeCode => $value];
        return $this->_urlBuilder->getUrl('*/*/*', ['_current' => true, '_use_rewrite' => true, '_query' => $query]);
    }
}
