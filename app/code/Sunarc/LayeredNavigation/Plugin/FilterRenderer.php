<?php
namespace Sunarc\LayeredNavigation\Plugin;

use Magento\Swatches\Model\Plugin\FilterRenderer as CoreRenderer;

/**
 * Class FilterRenderer
 */
class FilterRenderer extends CoreRenderer
{
    /**
     * Path to RenderLayered Block
     *
     * @var string
     */
    protected $block = 'Sunarc\LayeredNavigation\Block\LayeredNavigation\RenderLayered';
}
