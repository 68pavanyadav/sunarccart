<?php

namespace Sunarc\RatingFilter\Block\Filter;

use Magento\Framework\Registry;

class Rating extends \Magento\Framework\View\Element\Template
{
    public $registry;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context, Registry $registry)
    {
        $this->registry = $registry;
        parent::__construct($context);
    }

    public function _toHtml()
    {
        if ($this->_scopeConfig->isSetFlag('ratingfilter/general/enable') &&
            $this->_scopeConfig->isSetFlag('ratingfilter/general/isslider')
        ) {
            return parent::_toHtml();
        }
        return '';
    }

    public function getCurrentUrl()
    {
        $category = $this->registry->registry('current_category');
        $url = $category->getUrl();
        $parameters = $this->getRequest()->getParams();
        if (!empty($parameters)) {
            unset($parameters['rat']);
            $new_query_string = http_build_query($parameters);
            return $url . "?" . $new_query_string;
        }
        return $url;
    }

    public function getRatingPar()
    {
        $ratingPar = $this->getRequest()->getParam('rat');
        if ($ratingPar) {
            $filter = explode('-', $ratingPar);
            list($from, $to) = $filter;
            $fromRange = '';
            $toRange = '';
            switch ($from) {
                case '21':
                    $fromRange = '2';
                    break;
                case '41':
                    $fromRange = '3';
                    break;
                case '61':
                    $fromRange = '4';
                    break;
                case '81':
                    $fromRange = '5';
                    break;
                default:
                    $fromRange = '0';
                    break;
            }
            if ($to && $to != '00' && $to != 0) {
                $toRange = $to / (2 * 10);
            }
            $ranges = [];
            $ranges[] = $fromRange;
            $ranges[] = $toRange;
            return $ranges;
        }
        return false;
    }
}
