<?php

namespace Sunarc\RatingFilter\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{

    const CONFIG_PATH_MODULE_ENABLED = 'ratingfilter/general/enable';
    const CONFIG_PATH_SLIDER_ENABLED = 'ratingfilter/general/isslider';
    /*
     * check if module enabled
     */
    public function getConfigModuleEnabled()
    {
        return $this->scopeConfig->getValue(self::CONFIG_PATH_MODULE_ENABLED, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getConfigSliderEnabled()
    {
        return $this->scopeConfig->getValue(self::CONFIG_PATH_SLIDER_ENABLED, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
