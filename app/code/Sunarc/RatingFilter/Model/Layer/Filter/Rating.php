<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Sunarc\RatingFilter\Model\Layer\Filter;

use Magento\Framework\Registry;

/**
 * Layer category filter
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Rating extends \Magento\Catalog\Model\Layer\Filter\AbstractFilter
{

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    public $coreRegistry;

    /**
     * @var CategoryDataProvider
     */
    private $dataProvider;
    public $productCollectionFactory;
    public $registry;
    public $helper;

    /**
     * Construct
     *
     * @param \Magento\Catalog\Model\Layer\Filter\ItemFactory $filterItemFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\Layer $layer
     * @param \Magento\Catalog\Model\Layer\Filter\Item\DataBuilder $itemDataBuilder
     * @param CategoryFactory $categoryDataProviderFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Model\Layer\Filter\ItemFactory $filterItemFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Layer $layer,
        \Magento\Catalog\Model\Layer\Filter\Item\DataBuilder $itemDataBuilder,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Catalog\Model\Product $productModel,
        \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus,
        \Magento\Catalog\Model\Product\Visibility $productVisibility,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Sunarc\RatingFilter\Helper\Data $helper,
        Registry $registry,
        array $data = []
    ) {
    
        parent::__construct($filterItemFactory, $storeManager, $layer, $itemDataBuilder, $data);
        $this->objectManager = $objectManager;
        $this->_productModel = $productModel;
        $this->productStatus = $productStatus;
        $this->productVisibility = $productVisibility;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->registry = $registry;
        $this->helper = $helper;
        $this->_requestVar = 'rat';
    }

    /**
     * Get filter value for reset current filter state
     *
     * @return mixed|null
     */
    public function getResetValue()
    {
        return $this->dataProvider->getResetValue();
    }

    /**
     * Apply category filter to layer
     *
     * @param   \Magento\Framework\App\RequestInterface $request
     * @return  $this
     */
    public function apply(\Magento\Framework\App\RequestInterface $request)
    {
        $filter = $request->getParam($this->getRequestVar());
        if (!$filter) {
            return $this;
        }

        $filter = explode('-', $filter);
        list($from, $to) = $filter;
        $filterCollection = $this->getProductCollection();
        $filterCollection->getSelect()->joinLeft(['rova' => 'rating_option_vote_aggregated'], 'e.entity_id =rova.entity_pk_value', ["rova.percent"])
            ->where("rova.percent between " . $from . " and " . $to)
            ->group('e.entity_id');
        $entityIds = $filterCollection->getColumnValues('entity_id');
        $collection = $this->getLayer()->getProductCollection();
        $collection->addAttributeToFilter('entity_id', ['in' => $entityIds]);
        $collection->getSize();
        return $this;
    }

    public function getProductCollection()
    {
        $category = $this->registry->registry('current_category');
        $collection = $this->productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addCategoryFilter($category);
        $collection->addAttributeToFilter('visibility', \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH);
        $collection->addAttributeToFilter('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
        return $collection;
    }

    /**
     * Get filter name
     *
     * @return \Magento\Framework\Phrase
     */
    public function getName()
    {
        return __('Rating');
    }

    /**
     * Get data array for building attribute filter items
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return array
     */
    protected function _getItemsData()
    {
        if (!$this->helper->getConfigSliderEnabled()) {
            $s1 = '<div class="rating-summary" style="display: inline-block;margin-top: -5px;">
                                        <div class="rating-result" title="20%">
                                            <span style="width:20%"><span>1</span></span>
                                        </div>
                                    </div>';

            $s2 = '<div class="rating-summary" style="display: inline-block;margin-top: -5px;">
                                        <div class="rating-result" title="40%">
                                            <span style="width:40%"><span>2</span></span>
                                        </div>
                                    </div>';

            $s3 = '<div class="rating-summary" style="display: inline-block;margin-top: -5px;">
                                        <div class="rating-result" title="60%">
                                            <span style="width:60%"><span>3</span></span>
                                        </div>
                                    </div>';

            $s4 = '<div class="rating-summary" style="display: inline-block;margin-top: -5px;">
                                        <div class="rating-result" title="80%">
                                            <span style="width:80%"><span>4</span></span>
                                        </div>
                                    </div>';

            $s5 = '<div class="rating-summary" style="display: inline-block;margin-top: -5px;">
                                        <div class="rating-result" title="100%">
                                            <span style="width:100%"><span>5</span></span>
                                        </div>
                                    </div>';

            $facets = [
                '0-20' => $s1,
                '21-40' => $s2,
                '41-60' => $s3,
                '61-80' => $s4,
                '81-100' => $s5,
            ];

            $data = [];
            if (count($facets) > 1) {
                foreach ($facets as $key => $label) {
                    $count = 0;
                    $filter = explode('-', $key);
                    $count = $this->getProductsTotal($filter);
                    $this->itemDataBuilder->addItemData(
                        $label,
                        $key,
                        $count
                    );
                }
            }
        }
        return $this->itemDataBuilder->build();
    }


    public function getProductsTotal($filter)
    {
        list($from, $to) = $filter;
        $filterCollection = $this->getProductCollection();
        $filterCollection->getSelect()->joinLeft(['rova' => 'rating_option_vote_aggregated'], 'e.entity_id =rova.entity_pk_value', ["rova.percent"])
            ->where("rova.percent between " . $from . " and " . $to)
            ->group('e.entity_id');
        return $filterCollection->count();
    }
}
