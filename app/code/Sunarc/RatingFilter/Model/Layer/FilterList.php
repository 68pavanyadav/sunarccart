<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Sunarc\RatingFilter\Model\Layer;

class FilterList
{
    public $helper;

    public function __construct(
        \Sunarc\RatingFilter\Helper\Data $helper,
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) {
        $this->helper = $helper;
        $this->objectManager = $objectManager;
    }

    public function aroundGetFilters(\Magento\Catalog\Model\Layer\FilterList $subject, \Closure $proceed, \Magento\Catalog\Model\Layer $layer)
    {
        $result = $proceed($layer);
        if ($this->helper->getConfigModuleEnabled()) {
            $result[] = $this->objectManager->create('Sunarc\RatingFilter\Model\Layer\Filter\Rating', ['layer' => $layer]);
        }
        return $result;
    }
}
