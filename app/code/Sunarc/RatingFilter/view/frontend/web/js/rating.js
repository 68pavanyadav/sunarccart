/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

/* eslint-disable no-undef */
// jscs:disable jsDoc
require([
    'jquery',
    'jquery/ui',
    'jRange'
], function ($) {
    'use strict';
    $(function () {
        // function ratingFilter(position) {
        //     var rating = position.split(',');
        //     var fromRating = rating[0];
        //     var toRating = rating[1];
        //     var from = fromRating;
        //     var to = toRating;
        //     switch (fromRating) {
        //         case '2':
        //             from = '21';
        //             break;
        //         case '3':
        //             from = '41';
        //             break;
        //         case '4':
        //             from = '61';
        //             break;
        //         case '5':
        //             from = '81';
        //             break;
        //         default:
        //             from = '00';
        //             break;
        //     }
        //
        //     to = (to * 2) * 10;
        //
        //     if (to == 0 || to == '0') {
        //         to = '20';
        //     }
        //     var url = '<?php echo $currnetUrl; ?>';
        //     if (url.toLowerCase().indexOf("?") >= 0) {
        //         var newurl = url + "&rat=" + from + "-" + to;
        //     } else {
        //         var newurl = url + "?rat=" + from + "-" + to;
        //     }
        //     window.location.href = newurl;
        // }

        $('.catalog-rating-slider').jRange({
            from: 1,
            to: 5,
            step: 1,
            scale: [1, 2, 3, 4, 5],
            format: '%s',
            width: 200,
            showLabels: true,
            showScale: true,
            isRange: true,
            snap: true,
            ondragend: function (position) {
                //ratingFilter(position);
            },
            onbarclicked: function (position) {
                //ratingFilter(position);
            }
        });
    });
});