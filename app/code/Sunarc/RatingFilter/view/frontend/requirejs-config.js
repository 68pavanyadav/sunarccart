var config = {
    paths: {
        'jRange': "Sunarc_RatingFilter/js/rangeslider.min"
    },
    shim: {
        'jRange': {
            deps: ['jquery']
        }
    }
};